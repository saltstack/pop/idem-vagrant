def test_create(hub, instance_name):
    ret = hub.exec.vagrant.create(instance_name)
    assert ret == {}


def test_exists(hub, instance_name):
    ret = hub.exec.vagrant.exists(instance_name)
    assert ret is True


def test_list(hub, instance_name):
    ret = hub.exec.vagrant.list()
    assert instance_name in ret


def test_get(hub, instance_name):
    ret = hub.exec.vagrant.get(instance_name)
    assert ret == {}


def test_start(hub, instance_name):
    ret = hub.exec.vagrant.start(instance_name)
    assert ret == {}


def test_stop(hub, instance_name):
    ret = hub.exec.vagrant.stop(instance_name)
    assert ret == {}


def test_delete(hub, instance_name):
    ret = hub.exec.vagrant.delete(instance_name)
    assert ret == {}
