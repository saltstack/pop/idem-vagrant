corn>=6
idem>=7
idem-aix; os_name == "AIX"
idem-bsd; "BSD" in sys_platform
idem-darwin; sys_platform == "darwin"
idem-linux; sys_platform == "linux"
idem-solaris; "sunos" in sys_platform
idem-windows; sys_platform == 'win32'
pop>=13
pop-config>=6
